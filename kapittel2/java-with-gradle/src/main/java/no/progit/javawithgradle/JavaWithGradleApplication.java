package no.progit.javawithgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaWithGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaWithGradleApplication.class, args);
	}

}
