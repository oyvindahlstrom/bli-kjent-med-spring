package no.progit.kotlinwithmaven

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinWithMavenApplication

fun main(args: Array<String>) {
	runApplication<KotlinWithMavenApplication>(*args)
}
