package no.progit.kotlinwithgradle

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinWithGradleApplication

fun main(args: Array<String>) {
	runApplication<KotlinWithGradleApplication>(*args)
}
