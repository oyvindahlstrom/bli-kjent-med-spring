package no.progit.groovywithmaven

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class GroovyWithMavenApplication {

	static void main(String[] args) {
		SpringApplication.run(GroovyWithMavenApplication, args)
	}

}
