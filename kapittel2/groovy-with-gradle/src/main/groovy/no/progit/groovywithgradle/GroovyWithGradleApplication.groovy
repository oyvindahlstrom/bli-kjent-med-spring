package no.progit.groovywithgradle

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class GroovyWithGradleApplication {

	static void main(String[] args) {
		SpringApplication.run(GroovyWithGradleApplication, args)
	}

}
