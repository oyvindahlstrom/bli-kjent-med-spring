package no.progit.javawithmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaWithMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaWithMavenApplication.class, args);
	}

}
