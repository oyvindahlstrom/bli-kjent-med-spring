package no.progit.kapittel3.controller;

import no.progit.kapittel3.Domain.Todo;
import no.progit.kapittel3.service.TodoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TodoController.class)
public class TodoControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoService service;

    @Test
    void testGetTodos() throws Exception {
        Mockito.when(service.getTodos()).thenReturn(List.of(new Todo(1L, "testtodo", false)));
        mockMvc
            .perform(get("/todo"))
            .andExpect(status().isOk())
            .andExpect(view().name("todo"))
            .andExpect(content().string(containsString("Progit Thymeleaf TODO")))
            .andExpect(content().string(containsString("Description: testtodo")));
            
    }

    @Test
    void testAddTodos() throws Exception {
        Mockito.when(service.getTodos()).thenReturn(Collections.emptyList());
        mockMvc
                .perform(get("/todo/"))
                .andExpect(status().isOk())
                .andExpect(view().name("todo"))
                .andExpect(content().string(containsString("Progit Thymeleaf TODO")));

    }
}
