package no.progit.kapittel3.Domain;

public class Todo {

    private final Long id;
    private final String description;
    private final boolean finished;

    public Todo(Long id, String description, boolean finished) {
        this.id = id;
        this.description = description;
        this.finished = finished;
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFinished() {
        return finished;
    }
}
