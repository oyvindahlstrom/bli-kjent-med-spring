package no.progit.kapittel3.controller;

import no.progit.kapittel3.Domain.AddTodo;
import no.progit.kapittel3.Domain.Todo;
import no.progit.kapittel3.service.TodoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/todo")
public class TodoController {

	private final TodoService service;

	public TodoController(TodoService service) {
		this.service = service;
	}

	@GetMapping()
	public String viewTodos(Model model) {
		model.addAttribute("todos", service.getTodos());
		return "todo";
	}

	@GetMapping("/add")
	public String viewAddTodo(Model model) {
		model.addAttribute("addtodo", new AddTodo());
		return "add-todo";
	}

	@PostMapping("/add")
	public RedirectView addTodo(@ModelAttribute("addtodo") AddTodo addTodo, RedirectAttributes redirectAttributes) {
		final RedirectView redirectView = new RedirectView("/todo", true);
		Todo savedTodo = service.addTodo(addTodo);
		redirectAttributes.addFlashAttribute("savedTodo", savedTodo);
		redirectAttributes.addFlashAttribute("addTodoSuccess", true);
		return redirectView;
	}
}
