package no.progit.kapittel3.Domain;

public class AddTodo {

    private String description;

    public AddTodo() {}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
