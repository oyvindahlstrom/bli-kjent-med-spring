package no.progit.kapittel3.repository;

import no.progit.kapittel3.Domain.AddTodo;
import no.progit.kapittel3.Domain.Todo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TodoRepository {

    private volatile Long uniqueIdentity = 0L;
    private static final List<Todo> todos = new ArrayList<>();

    public List<Todo> getTodos() {
        return todos;
    }

    public Todo addTodo(AddTodo addTodo) {
        Todo todo = new Todo(getNextUniqueId(), addTodo.getDescription(), false);
        todos.add(todo);
        return todo;
    }

    private synchronized Long getNextUniqueId() {
        return ++uniqueIdentity;
    }
}
