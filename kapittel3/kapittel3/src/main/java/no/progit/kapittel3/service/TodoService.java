package no.progit.kapittel3.service;

import no.progit.kapittel3.Domain.AddTodo;
import no.progit.kapittel3.Domain.Todo;
import no.progit.kapittel3.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepository repository;

    public TodoService(TodoRepository repository) {
        this.repository = repository;
    }

    public List<Todo> getTodos() {
        return repository.getTodos();
    }

    public Todo addTodo(AddTodo addTodo) {
        return repository.addTodo(addTodo);
    }
}
