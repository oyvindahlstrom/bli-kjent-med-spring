package no.progit.kapittel3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kapittel3Application {

	public static void main(String[] args) {
		SpringApplication.run(Kapittel3Application.class, args);
	}

}
