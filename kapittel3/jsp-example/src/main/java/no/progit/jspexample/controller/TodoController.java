package no.progit.jspexample.controller;

import no.progit.jspexample.domain.AddTodo;
import no.progit.jspexample.domain.Todo;
import no.progit.jspexample.service.TodoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/todo")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/view")
    public String viewTodos(Model model) {
        model.addAttribute("todo", todoService.getTodos());
        return "view-todo";
    }

    @GetMapping("/add")
    public String viewAddTodo(Model model) {
        model.addAttribute("add-todo", new Todo());
        return "add-todo";
    }

    @PostMapping("/add")
    public RedirectView addTodo(@ModelAttribute("add-todo") AddTodo addTodo, RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/todo/view", true);
        Todo savedTodo = todoService.addTodo(new Todo(addTodo.getName()));
        redirectAttributes.addFlashAttribute("savedTodo", savedTodo);
        redirectAttributes.addFlashAttribute("addTodoSuccess", true);
        return redirectView;
    }
}
