package no.progit.jspexample.domain;

public class Todo {

    private Long id;

    private String name;

    private boolean finished;

    public Todo() {}

    public Todo(String name) {
        this.id = null;
        this.name = name;
        this.finished = false;
    }

    public Todo(String name, boolean finished) {
        this.id = null;
        this.name = name;
        this.finished = finished;
    }

    public Todo(Long id, String name, boolean finished) {
        this(name, finished);
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public boolean isFinished() {
        return finished;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
