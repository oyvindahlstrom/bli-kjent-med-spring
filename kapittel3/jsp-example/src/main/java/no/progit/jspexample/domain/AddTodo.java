package no.progit.jspexample.domain;

public class AddTodo {

    private final String name;

    public AddTodo(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }
}
