package no.progit.jspexample.service;

import no.progit.jspexample.domain.Todo;
import no.progit.jspexample.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> getTodos() {
        return todoRepository.getTodos();
    }

    public Todo addTodo(Todo todo) {
        return todoRepository.addTodo(todo);
    }
}
