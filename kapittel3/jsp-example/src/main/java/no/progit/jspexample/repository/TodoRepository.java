package no.progit.jspexample.repository;

import no.progit.jspexample.domain.Todo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TodoRepository {

    private volatile Long uniqueIdentity = 0L;

    private final List<Todo> todos = new ArrayList<>();

    public List<Todo> getTodos() {
        return todos;
    }

    public Todo addTodo(Todo todo) {
        todo.setId(getNextUniqueId());
        this.todos.add(todo);
        return todo;
    }

    private synchronized Long getNextUniqueId() {
        return ++uniqueIdentity;
    }
}
