<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>JSP todo's</title>
    <link href="<c:url value="/css/common.css"/>" rel="stylesheet" type="text/css">
</head>
<body>
<c:if test="${addTodoSuccess}">
    <div>Successfully added Todo with ID: ${savedTodo.id}</div>
</c:if>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Finished</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${todo}" var="todo">
        <tr>
            <td>${todo.id}</td>
            <td>${todo.name}</td>
            <td>${todo.finished}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<c:url value="/todo/add" var="addTodoPageUrl" />
<a href=${addTodoPageUrl} >
    <button>Add new TODO</button>
</a>
</body>
</html>