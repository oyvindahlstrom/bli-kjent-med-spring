<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Todo</title>
</head>
<body>
<c:url var="add_todo_url" value="/todo/add"/>
<form:form action="${add_todo_url}" method="post" modelAttribute="add-todo">
    <form:label path="name">Name: </form:label>
    <form:input type="text" path="name" name="name" />
    <input type="submit" value="submit"/>
</form:form>
</body>
</html>